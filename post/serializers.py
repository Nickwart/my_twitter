from rest_framework import serializers

from post.models import UserProfile, Post, Like


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ["user", "last_login", "last_request"]


class LikeSerializer(serializers.ModelSerializer):
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all())

    class Meta:
        model = Like
        fields = ["id", "post", "user", "created"]
        read_only_fields = ["created"]


class PostCreateSerializer(serializers.ModelSerializer):
    body = serializers.CharField(max_length=255)

    class Meta:
        model = Post
        fields = ["id", "body", "creator"]
        read_only_fields = ["creator"]
