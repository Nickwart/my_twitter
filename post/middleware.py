import datetime

from django.utils.deprecation import MiddlewareMixin


class SetLastVisitMiddleware(MiddlewareMixin):
    def process_response(self, request, response):
        if request.user.is_authenticated:
            request.user.profile.last_request = datetime.datetime.now()
            request.user.profile.save()
        return response
