import datetime

from django.db.models import Count
from django.http import HttpResponse
from drf_registration.api import RegisterView
from drf_registration.utils.domain import get_current_domain
from drf_registration.utils.email import send_verify_email, send_email_welcome
from drf_registration.utils.users import (
    get_user_profile_data,
    has_user_activate_token,
    has_user_verify_code,
)
from rest_framework.generics import GenericAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework import mixins, status
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.views import TokenObtainPairView

from post.models import Post, Like, UserProfile
from post.serializers import PostCreateSerializer, LikeSerializer, UserProfileSerializer


class PostCreateAPIView(CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostCreateSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(creator=self.request.user)


class LikeCreateAPIView(mixins.CreateModelMixin, GenericAPIView):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        request.data["user"] = self.request.user.pk
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    def perform_create(self, serializer):
        serializer.save()


class LikeDestroyAPIView(mixins.DestroyModelMixin, GenericAPIView):
    queryset = Like.objects.all()
    serializer_class = LikeSerializer
    permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class LikesCountAPIView(GenericAPIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        date_from = self.request.query_params.get("date_from")
        date_to = self.request.query_params.get("date_to")
        user = self.request.user
        statistics = (
            Like.objects.filter(user=user, created__gte=date_from, created__lte=date_to)
            .values("created__date")
            .annotate(count=Count("id"))
            .values("created__date", "count")
            .order_by("created__date")
        )
        return HttpResponse(statistics)


class UserDataAPIView(GenericAPIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        serializer = UserProfileSerializer(self.request.user.profile)
        return Response(serializer.data)


class CustomTokenObtainPairView(TokenObtainPairView):
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])
        serializer.user.profile.last_login = datetime.datetime.now()
        serializer.user.profile.save()
        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class CustomRegisterView(RegisterView):

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = serializer.save()
        UserProfile.objects.create(user=user)
        data = get_user_profile_data(user)

        domain = get_current_domain(request)

        if has_user_activate_token() or has_user_verify_code():
            send_verify_email(user, domain)
        else:
            send_email_welcome(user)

        return Response(data, status=status.HTTP_201_CREATED)
